package ua.com.mitk.bibleversus;


import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.DisplayMetrics;

/**
 * Class provides static methods and variables
 */

public class UtilsApp {

//    id favorite theme in database
    static final int FAVORITE_THEME_ID = 12;

// Array of backdrop images
    public static int[] arrImages = {R.drawable.courage,R.drawable.death,R.drawable.forgiveness,
            R.drawable.faith, R.drawable.family,  R.drawable.encoragement,
            R.drawable.friendship, R.drawable.joy, R.drawable.life,
            R.drawable.love, R.drawable.relationship, R.drawable.strength,
            R.drawable.favorites};

    /** Decodes image to bitmap. Resolves an issue with memory */
    public static Bitmap decodeSampledBitmapFromDrawable(Context activity, int res, int reqWidth, int reqHeight) {
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(activity.getResources(), res , options);
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
        options.inJustDecodeBounds = false;
        options.inPreferredConfig = Bitmap.Config.RGB_565;
        options.inScaled = false;
        return BitmapFactory.decodeResource(activity.getResources(), res, options);
    }

    public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;
        if (height > reqHeight || width > reqWidth) {
            final int halfHeight = height / 2;
            final int halfWidth = width / 2;
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }
        return inSampleSize;
    }


    /** Returns device screen width */
    public static int parentWidth(Context context){
        return  context.getApplicationContext().getResources().getDisplayMetrics().widthPixels;
    }

    /** Returns device screen height */
    public static int parentHeight(Context context){
        return context.getApplicationContext().getResources().getDisplayMetrics().heightPixels;
    }


    /** Converts Dp to Pixels */
    public static int convertDpToPixel(float dp, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        int px = Math.round(dp * (metrics.densityDpi / 160f));
        return px;
    }


}
