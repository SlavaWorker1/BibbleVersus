package ua.com.mitk.bibleversus;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;

import com.twitter.sdk.android.core.identity.TwitterAuthClient;

import ua.com.mitk.bibleversus.database.Theme;

/**
 * Activity contains elements
 * thats display main content of app, Bibble versus
 */
public class ScrollingActivity extends AppCompatActivity {

    final int PAGE_COUNT = Theme.getAll().size()-1;
    MyFragmentPagerAdapter myFragmentPagerAdapter;
    ViewPager viewPager;
    static TwitterAuthClient twitterAuthClient;


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Fragment fragment = myFragmentPagerAdapter.getItem(0);
        if (fragment != null) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
        twitterAuthClient.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(
                WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN
        );
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scrolling);

        twitterAuthClient = new TwitterAuthClient();

        final Long themeId = getIntent().getLongExtra("theme_id", -1);

        viewPager = (ViewPager)findViewById(R.id.viewPager);
        myFragmentPagerAdapter = new MyFragmentPagerAdapter(getSupportFragmentManager());
        PagerAdapter pagerAdapter = myFragmentPagerAdapter;
        viewPager.setAdapter(pagerAdapter);
        // setOffsceenPageLimit 2, for animations without lags
        viewPager.setOffscreenPageLimit(2);
        viewPager.setPageTransformer(false, new MyAccordionTransformer());
        viewPager.setCurrentItem(Integer.valueOf(String.valueOf(themeId)));

    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private class MyFragmentPagerAdapter extends FragmentPagerAdapter {

        public MyFragmentPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return PageFragment.newInstance(position);
        }

        @Override
        public int getItemPosition(Object object) {
            return super.getItemPosition(object);
        }

        @Override
        public int getCount() {
            return PAGE_COUNT;
        }
    }

}
