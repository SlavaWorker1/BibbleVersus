package ua.com.mitk.bibleversus;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.util.List;

import ua.com.mitk.bibleversus.database.Theme;
import ua.com.mitk.bibleversus.database.Versus;

/**
 * Fragment thats contain bibble versus
 */
public class PageFragment extends Fragment {

    static final String ARGUMENT_PAGE_NUMBER = "arg_page_number";


    private int mPageNumber;
    private String[] arrToolbarColors;
    private String[] arrToolbarTxtColors;
    private String[] arrToolbarLightTxtColors;



    public PageFragment() {
        // Required empty public constructor
    }

    static PageFragment newInstance(int page) {
        PageFragment pageFragment = new PageFragment();
        Bundle arguments = new Bundle();
        arguments.putInt(ARGUMENT_PAGE_NUMBER, page);
        pageFragment.setArguments(arguments);
        return pageFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPageNumber = getArguments().getInt(ARGUMENT_PAGE_NUMBER);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        ScrollingActivity.twitterAuthClient.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        arrToolbarColors = getResources().getStringArray(R.array.colorsCollapsingToolbarArray);
        arrToolbarTxtColors = getResources().getStringArray(R.array.colorsTextCollapsingToolbararray);
        arrToolbarLightTxtColors = getResources().getStringArray(R.array.colorsTextWhitesToolbarArray);

        View rootView = inflater.inflate(R.layout.fragment_scrolling_content, null);
        
        // Adds back button to ActionBar
        setToolbarBackButton(rootView);
        
        Theme theme = Theme.load(Theme.class, mPageNumber);

        ImageView imgBackdrop = (ImageView)rootView.findViewById(R.id.backdrop);
        Bitmap bmpTopBackdrop = UtilsApp.decodeSampledBitmapFromDrawable(
                getActivity(),
                UtilsApp.arrImages[(Integer.valueOf(String.valueOf(theme.getId())))],
                imgBackdrop.getMaxWidth(),
                imgBackdrop.getMaxHeight()
        );
        imgBackdrop.setImageBitmap(bmpTopBackdrop);

        // Sets collapsingTollbar
        setUpCollapsingToolbar(rootView, theme);

        LinearLayout linearLayout = (LinearLayout)rootView.findViewById(R.id.cardLayout);
        List<Versus> versusList = theme.versuses();

        LinearLayout.LayoutParams cardsParams = new AppBarLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
        );


        for (int i = 0; i < versusList.size(); i++) {
            CustomCard customCard = new CustomCard(getActivity(), versusList.get(i), linearLayout);
            customCard.setLayoutParams(cardsParams);
            linearLayout.addView(customCard);
        }

        return rootView;
    }

    private void setToolbarBackButton(View rootView) {
        Toolbar toolbar = (Toolbar)rootView.findViewById(R.id.toolbar);
        
        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
        
        if (((AppCompatActivity) getActivity()).getSupportActionBar() != null ) {
            ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);
            ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            ((AppCompatActivity) getActivity()).getSupportActionBar().setHomeButtonEnabled(true);
            final Drawable upArrow = ContextCompat.getDrawable(
                    getActivity(),
                    R.drawable.abc_ic_ab_back_mtrl_am_alpha
            );
            upArrow.setColorFilter(
                    ContextCompat.getColor(
                            getActivity(),
                            R.color.white_color
                    ),
                    PorterDuff.Mode.SRC_ATOP
            );
            ((AppCompatActivity) getActivity()).getSupportActionBar().setHomeAsUpIndicator(upArrow);
        }
    }
    

    private void setUpCollapsingToolbar(View view, Theme theme) {
        CollapsingToolbarLayout collapsingToolbarLayout;
        collapsingToolbarLayout = (CollapsingToolbarLayout)view.findViewById(R.id.collapsing_toolbar);
        collapsingToolbarLayout.setTitle(theme.getTheme());

        // Set up CollapsingToolbar colors
        setToolbarColor(collapsingToolbarLayout);
    }

    private void setToolbarColor(CollapsingToolbarLayout collapsingToolbarLayout) {
        collapsingToolbarLayout.setContentScrimColor(
                Color.parseColor(arrToolbarColors[getmPageNumber()])
        );
        collapsingToolbarLayout.setCollapsedTitleTextColor(
                Color.parseColor(arrToolbarTxtColors[getmPageNumber()])
        );
        collapsingToolbarLayout.setExpandedTitleColor(
                Color.parseColor(arrToolbarLightTxtColors[getmPageNumber()])
        );
    }


    public int getmPageNumber() {
        return mPageNumber;
    }
}
