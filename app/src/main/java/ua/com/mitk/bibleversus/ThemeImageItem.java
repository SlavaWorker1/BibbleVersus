package ua.com.mitk.bibleversus;


import android.content.Context;
import android.graphics.Bitmap;
import android.util.AttributeSet;
import android.view.Gravity;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Custom View for display theme image with text in MainFragment gridLayout
 */
public class ThemeImageItem extends FrameLayout {

    private int mParentWidth;
    private int mParentHeight;
    private Context mContext;
    private ImageView mImageView;
    TextView tvThemeName;

    public ThemeImageItem(Context context) {
        super(context);
        init(context);
    }

    public ThemeImageItem(Context context, AttributeSet attrs, int mParentWidth) {
        super(context, attrs);
        init(context);
    }

    private void init(Context context) {
        mContext = context;
        mParentHeight = UtilsApp.parentHeight(context);
        mParentWidth = UtilsApp.parentWidth(context);
    }

    /**
     *  Adds text to ThemeImageItem
     */
    public void addText(String text) {
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(
            mParentWidth/3-2, // 3 colums
            LayoutParams.WRAP_CONTENT
        );
        tvThemeName = new TextView(mContext);
        tvThemeName.setText(text);
        setTextSize(tvThemeName);

        layoutParams.gravity = Gravity.BOTTOM;
        tvThemeName.setGravity(Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL);
        tvThemeName.setLayoutParams(layoutParams);
        tvThemeName.setPadding(20, 10, 20, 10);
        this.addView(tvThemeName);
    }

    /**
     * Adds image to ThemeImageItem
     */
    public void addImage(Bitmap btmThemeImage) {
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(
            mParentWidth/3 -2, // 3 colums
            mParentHeight/4    // 4 lines
        );
        mImageView = new ImageView(mContext);
        mImageView.setImageBitmap(btmThemeImage);
        mImageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        layoutParams.gravity = Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL;
        mImageView.setLayoutParams(layoutParams);
        this.addView(mImageView);
    }

    /**
     * Sets size of text depends of screen width
     */
    private void setTextSize(TextView tvThemeName) {
        if (mParentWidth < 500) {
            tvThemeName.setTextSize(9);
        } else if (mParentWidth > 1000){
            tvThemeName.setTextSize(12);
        } else {
            tvThemeName.setTextSize(10);
        }
    }

    public int getmParentWidth() {
        return mParentWidth;
    }

    public int getmParentHeight() {
        return mParentHeight;
    }

    public TextView getTvThemeName() {
        return tvThemeName;
    }
}
