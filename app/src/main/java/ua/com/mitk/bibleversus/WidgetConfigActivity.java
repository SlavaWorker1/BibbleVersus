package ua.com.mitk.bibleversus;

import android.app.Activity;
import android.appwidget.AppWidgetManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.activeandroid.ActiveAndroid;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import ua.com.mitk.bibleversus.database.Theme;
import ua.com.mitk.bibleversus.database.Versus;

public class WidgetConfigActivity extends Activity {

    int widgetID = AppWidgetManager.INVALID_APPWIDGET_ID;
    Intent resultValue;
    final String LOG_TAG = "config";
    static List<String> poemsList;
    Spinner spinner;
    Button btnRandomPoem;
    TextView tvPoem;

    public final static String WIDGET_PREF = "widget_pref";
    public final static String WIDGET_TEXT = "widget_text";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(LOG_TAG, "onCreate config");
        ActiveAndroid.initialize(this);
        Theme theme = Theme.load(Theme.class, UtilsApp.FAVORITE_THEME_ID);
        List<Versus> versusList = theme.versuses();
        poemsList = new ArrayList<>();
        for (int i = 0; i < versusList.size(); i++) {
            poemsList.add(versusList.get(i).versusContent);
        }

        Intent intent = getIntent();
        Bundle extras = intent.getExtras();

        if (extras != null) {
            widgetID = extras.getInt(
                    AppWidgetManager.EXTRA_APPWIDGET_ID,
                    AppWidgetManager.INVALID_APPWIDGET_ID
            );
        }
        // и проверяем его корректность
        if (widgetID == AppWidgetManager.INVALID_APPWIDGET_ID) {
            finish();
        }
        // формируем intent ответа
        resultValue = new Intent();
        resultValue.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, widgetID);

        // отрицательный ответ
        setResult(RESULT_CANCELED, resultValue);
        setContentView(R.layout.activity_widget_config);

        final SpinnerAdapter myAdapter = new SpinnerAdapter(this, poemsList);

        spinner = (Spinner) findViewById(R.id.spinerPoem);
        spinner.setAdapter(myAdapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // показываем позиция нажатого элемента
                Toast.makeText(getBaseContext(), "Position = " + position, Toast.LENGTH_SHORT).show();
                spinner.getAdapter().getView(position, view, parent).setSelected(true);
                myAdapter.notifyDataSetChanged();
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });
        btnRandomPoem = (Button)findViewById(R.id.btnPoem);
        tvPoem = (TextView)findViewById(R.id.tvPoem);
        btnRandomPoem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tvPoem.setText(spinner.getSelectedItem().toString());
                SharedPreferences sp = getSharedPreferences(WIDGET_PREF, MODE_PRIVATE);
                SharedPreferences.Editor editor = sp.edit();
                editor.putString(WIDGET_TEXT + widgetID, tvPoem.getText().toString());
                editor.commit();
                AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(WidgetConfigActivity.this);
                MyWidget.updateWidget(WidgetConfigActivity.this, appWidgetManager, widgetID);
                setResult(RESULT_OK, resultValue);
                Log.d(LOG_TAG, "finish config " + widgetID);
                finish();
            }
        });

    }

}
