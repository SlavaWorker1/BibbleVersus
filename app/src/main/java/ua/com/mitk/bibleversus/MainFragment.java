package ua.com.mitk.bibleversus;

import android.app.Fragment;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v7.graphics.Palette;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridLayout;
import android.widget.ImageView;

import com.twitter.sdk.android.core.identity.TwitterLoginButton;

import java.util.List;

import ua.com.mitk.bibleversus.database.Theme;

public class MainFragment extends Fragment implements View.OnClickListener {


    TwitterLoginButton loginButton;

    public MainFragment() {
        // Required empty public constructor
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        loginButton.onActivityResult(requestCode,resultCode,data);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_main, container, false);

        CollapsingToolbarLayout collapsingToolbarLayout;
        collapsingToolbarLayout = (CollapsingToolbarLayout)rootView.findViewById(R.id.collapsing_toolbar_Main);
        collapsingToolbarLayout.setTitle(getString(R.string.bibble_verses));
        collapsingToolbarLayout.setTitleEnabled(true);

//        themes list from database
        List<Theme> themeList = Theme.getAll();

        GridLayout gridLayoutImgContainer = (GridLayout)rootView.findViewById(R.id.gridLayout) ;
        FloatingActionButton fabFavorites = (FloatingActionButton)rootView.findViewById(R.id.fabFavorite);
        fabFavorites.setOnClickListener(this);

//        Image on the top
        ImageView imgBackdrop = (ImageView)rootView.findViewById(R.id.imgBackdrop);
        Bitmap bmpBackdrop = UtilsApp.decodeSampledBitmapFromDrawable(
                getActivity(),
                R.drawable.backdrop,
                imgBackdrop.getMaxWidth(),
                imgBackdrop.getMaxHeight()
        );
        imgBackdrop.setImageBitmap(bmpBackdrop);

        collapsingToolbarLayout.setContentScrimColor(ContextCompat.getColor(getActivity(), R.color.mainActionBar));
        collapsingToolbarLayout.setCollapsedTitleTextColor(ContextCompat.getColor(
                getActivity(),
                R.color.main_titles));
        collapsingToolbarLayout.setExpandedTitleColor(ContextCompat.getColor(
                getActivity(),
                R.color.main_titles_white
        ));

        addThemeImages(themeList, gridLayoutImgContainer);

        return rootView;
    }


    /**
     * Adds items to gridLayout
     * @param themeList list theme for text descriptions
     * @param gridLayoutImgContainer container for ThemeImageItems
     */
    private void addThemeImages(List<Theme> themeList, GridLayout gridLayoutImgContainer) {
        for (int i = 0; i < themeList.size()-1; i++) {
            ThemeImageItem themeImageItem = new ThemeImageItem(getActivity());

            if ((i - 1) % 3 == 0) {
                themeImageItem.setPadding(4, 2, 4, 2); // set paddings to central item
            } else {
                themeImageItem.setPadding(0, 2, 0, 2); // set padding to
            }

            themeImageItem.setBackgroundColor(ContextCompat.getColor(
                    getActivity(),
                    R.color.primaryDark
            ));

            Bitmap bmpThemeImage = UtilsApp.decodeSampledBitmapFromDrawable(
                    getActivity(),
                    UtilsApp.arrImages[i],
                    themeImageItem.getmParentWidth(),
                    themeImageItem.getmParentHeight()
            );
            themeImageItem.addImage(bmpThemeImage);
            themeImageItem.addText(themeList.get(i).theme);

            themeImageItem.getTvThemeName().setBackgroundColor((ContextCompat.getColor(
                    getActivity(),
                    R.color.primary)));
            themeImageItem.getTvThemeName().setTextColor((ContextCompat.getColor(
                    getActivity(),
                    R.color.main_titles)));

            themeImageItem.setOnClickListener(this);
            themeImageItem.setId(R.id.themeImageItem);
            themeImageItem.setTag(themeList.get(i).getId());
            gridLayoutImgContainer.addView(themeImageItem);
        }
    }


    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()){
            case R.id.fabFavorite:
                intent = new Intent(getActivity(), FavoritesActivity.class);
                intent.putExtra("theme_id", Long.valueOf(UtilsApp.FAVORITE_THEME_ID));
                startActivity(intent);
                break;
            case R.id.themeImageItem:
                intent = new Intent(getActivity(), ScrollingActivity.class);
                intent.putExtra("theme_id", (Long) v.getTag());
                startActivity(intent);
                break;
        }

    }
}
