package ua.com.mitk.bibleversus.database;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.util.List;

@Table(name = "versus", id = "_id")
public class Versus extends Model {

    public Versus() {
    }

    @Column (name = "versusContent")
    public String versusContent;

    @Column (name = "theme_id")
    public String themeId;

    @Column (name = "isLiked")
    public int isLiked;

    public void setIsLiked(int isLiked) {
        this.isLiked = isLiked;
    }

    public static List<Versus> getLiked() {
        return new Select()
                .from(Versus.class)
                .where("isLiked = ?", 1)
                .orderBy("_id")
                .execute();
    }


}
