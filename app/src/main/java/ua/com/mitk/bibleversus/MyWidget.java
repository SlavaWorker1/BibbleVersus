package ua.com.mitk.bibleversus;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.widget.RemoteViews;

import java.util.Arrays;
import java.util.Random;

/**
 * Created by slava on 15.11.16.
 */
public class MyWidget extends AppWidgetProvider {

    final static String LOG_TAG = "myLogs";
    final static String ACTION_CHANGE = "ua.com.mitk.bibleversus.new_poem";

    @Override
    public void onEnabled(Context context) {
        super.onEnabled(context);
        Log.d(LOG_TAG, "onEnabled");
    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager,
                         int[] appWidgetIds) {
        super.onUpdate(context, appWidgetManager, appWidgetIds);
        Log.d(LOG_TAG, "onUpdate " + Arrays.toString(appWidgetIds));
        for (int id : appWidgetIds) {
            updateWidget(context, appWidgetManager, id);
        }
    }

    @Override
    public void onDeleted(Context context, int[] appWidgetIds) {
        super.onDeleted(context, appWidgetIds);
        Log.d(LOG_TAG, "onDeleted " + Arrays.toString(appWidgetIds));
        SharedPreferences.Editor editor = context.getSharedPreferences(
                WidgetConfigActivity.WIDGET_PREF, Context.MODE_PRIVATE
        ).edit();
        for (int widgetID : appWidgetIds) {
            editor.remove(WidgetConfigActivity.WIDGET_TEXT + widgetID);
        }
        editor.commit();
    }

    @Override
    public void onDisabled(Context context) {
        super.onDisabled(context);
        Log.d(LOG_TAG, "onDisabled");
    }

    static void updateWidget(Context context, AppWidgetManager appWidgetManager,
                            int widgetID) {
        Log.d(LOG_TAG, "updateWidget " + widgetID);

        SharedPreferences sp = context.getSharedPreferences(WidgetConfigActivity.WIDGET_PREF, Context.MODE_PRIVATE);
        // Читаем параметры Preferences
        String widgetText = sp.getString(WidgetConfigActivity.WIDGET_TEXT + widgetID, null);
        if (widgetText == null) return;
        // Настраиваем внешний вид виджета
        RemoteViews widgetView = new RemoteViews(
                context.getPackageName(),
                R.layout.widget_layout
        );
        widgetView.setTextViewText(R.id.tvBiblePoem, widgetText);
        // Обновляем виджет

        PendingIntent pIntent;
        // Открыть окно настроек
        Intent configIntent = new Intent(context, WidgetConfigActivity.class);
        configIntent.setAction(AppWidgetManager.ACTION_APPWIDGET_CONFIGURE);
        configIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, widgetID);
        pIntent = PendingIntent.getActivity(context, widgetID, configIntent, 0);
        widgetView.setOnClickPendingIntent(R.id.tvBiblePoem, pIntent);

        appWidgetManager.updateAppWidget(widgetID, widgetView);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        super.onReceive(context, intent);
        if (intent.getAction().equalsIgnoreCase(ACTION_CHANGE)) {

            // извлекаем ID экземпляра
            int mAppWidgetId = AppWidgetManager.INVALID_APPWIDGET_ID;
            Bundle extras = intent.getExtras();
            if (extras != null) {
                mAppWidgetId = extras.getInt(
                        AppWidgetManager.EXTRA_APPWIDGET_ID,
                        AppWidgetManager.INVALID_APPWIDGET_ID);

            }
            if (mAppWidgetId != AppWidgetManager.INVALID_APPWIDGET_ID) {
                // Читаем значение счетчика, увеличиваем на 1 и записываем
                SharedPreferences sp = context.getSharedPreferences(
                        WidgetConfigActivity.WIDGET_PREF, Context.MODE_PRIVATE
                );
                sp.edit().putString(
                        WidgetConfigActivity.WIDGET_TEXT + mAppWidgetId,
                        WidgetConfigActivity.poemsList.get(new Random().nextInt(WidgetConfigActivity.poemsList.size()))
                ).commit();

                // Обновляем виджет
                updateWidget(context, AppWidgetManager.getInstance(context), mAppWidgetId);
            }
        }
    }
}
