package ua.com.mitk.bibleversus;

import android.graphics.Bitmap;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.util.List;

import ua.com.mitk.bibleversus.database.Theme;
import ua.com.mitk.bibleversus.database.Versus;

public class FavoritesActivity extends AppCompatActivity {

    LinearLayout linearLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(
                WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN
        );
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorites);
        setToolbarBackButton();
        CollapsingToolbarLayout collapsingToolbarLayout;
        collapsingToolbarLayout = (CollapsingToolbarLayout)findViewById(R.id.collapsing_toolbar_Fav);
        collapsingToolbarLayout.setTitle(getString(R.string.favorites));
        collapsingToolbarLayout.setContentScrimColor(
                ContextCompat.getColor(this, R.color.favorite_toolbar)
        );
        collapsingToolbarLayout.setCollapsedTitleTextColor(
                ContextCompat.getColor(this, R.color.favorite_txt)
        );
        collapsingToolbarLayout.setExpandedTitleColor(
                ContextCompat.getColor(this, R.color.favorite_txt_light)
        );

        Theme theme = Theme.load(Theme.class, UtilsApp.FAVORITE_THEME_ID);

        ImageView imgBackdrop = (ImageView)findViewById(R.id.imgBackdropFav);
        Bitmap bmpTopBackdrop = UtilsApp.decodeSampledBitmapFromDrawable(
                this,
                UtilsApp.arrImages[(Integer.valueOf(String.valueOf(theme.getId())))],
                imgBackdrop.getMaxWidth(),
                imgBackdrop.getMaxHeight()
        );
        imgBackdrop.setImageBitmap(bmpTopBackdrop);

        linearLayout = (LinearLayout)findViewById(R.id.cardLayout);

        LinearLayout.LayoutParams cardsParams = new AppBarLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
        );

        List<Versus> versusList = theme.versuses();

        if (versusList.size() == 0) {
            CustomCard customCard = new CustomCard(
                    this,
                    getString(R.string.empty_favorites),
                    linearLayout
            );
            linearLayout.addView(customCard);


        } else {

            for (int i = 0; i < versusList.size(); i++) {
                CustomCard customCard = new CustomCard(this, versusList.get(i), linearLayout);
                customCard.setLayoutParams(cardsParams);
                linearLayout.addView(customCard);
            }
        }
    }

    private void setToolbarBackButton() {
        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbarFav);

        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null ) {
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
            final Drawable upArrow = ContextCompat.getDrawable(
                    this,
                    R.drawable.abc_ic_ab_back_mtrl_am_alpha
            );
            upArrow.setColorFilter(
                    ContextCompat.getColor(
                            this,
                            R.color.white_color
                    ),
                    PorterDuff.Mode.SRC_ATOP
            );
            getSupportActionBar().setHomeAsUpIndicator(upArrow);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
}
