package ua.com.mitk.bibleversus;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.activeandroid.query.Delete;
import com.activeandroid.query.Update;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.models.Tweet;
import com.twitter.sdk.android.core.services.StatusesService;

import ua.com.mitk.bibleversus.database.Theme;
import ua.com.mitk.bibleversus.database.Versus;


public class CustomCard extends CardView implements View.OnClickListener{

    private String mShareText;
    private TextView tvVersusText;
    private ImageView imgLike, imgShare;
    private boolean isClicked = false;
    private LinearLayout linearLayoutParentCard;
    private Theme mFavoriteTheme;
    private Versus mVersus;
    private Context mContext;
    private ImageView customTwitterButton;


    public CustomCard(Context context, Versus versus, LinearLayout linearLayout) {
        super(context);
        this.mContext = context;
        this.mShareText = versus.versusContent;
        this.mVersus = versus;
        this.mFavoriteTheme = Theme.load(Theme.class, UtilsApp.FAVORITE_THEME_ID);
        this.linearLayoutParentCard = linearLayout;
        init();

    }

    public CustomCard(Context context, String text, LinearLayout linearLayout) {
        super(context);
        this.mContext = context;
        this.mShareText  = text;
        this.linearLayoutParentCard = linearLayout;
        initFav();
    }

    private void initFav() {
        inflate(mContext, R.layout.custom_empty_card, this);
        tvVersusText = (TextView)findViewById(R.id.tvText);
        tvVersusText.setTextColor(ContextCompat.getColor(mContext, R.color.favorite_txt));
        tvVersusText.setText(mShareText);
    }

    private void init() {
        inflate(mContext, R.layout.custom_card, this);
        tvVersusText = (TextView)findViewById(R.id.tvText);
        tvVersusText.setText(mVersus.versusContent);
        imgLike = (ImageView)findViewById(R.id.imgLike);
        customTwitterButton = (ImageView)findViewById(R.id.imgTwitt);
        customTwitterButton.setVisibility(GONE);
        customTwitterButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ScrollingActivity.twitterAuthClient.authorize((Activity) mContext, new Callback<TwitterSession>() {
                    @Override
                    public void success(Result<TwitterSession> result) {
                        publishTweet(tvVersusText.getText().toString());
                        Toast.makeText(mContext, "Success", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void failure(TwitterException e) {
                        Toast.makeText(mContext, "Fail", Toast.LENGTH_SHORT).show();

                    }
                });
            }
        });
        imgLike.setOnClickListener(this);
        imgLike.setTag(mVersus);
        imgShare = (ImageView)findViewById(R.id.imgShare);
        imgShare.setOnClickListener(this);

        if (mVersus.isLiked == 1) {
            imgLike.setImageResource(R.mipmap.ic_favorite_on);
        } else {
            imgLike.setImageResource(R.mipmap.ic_favorite_off);
        }
        isClicked = (mVersus.isLiked == 1);

    }

    public CustomCard(Context context) {
        super(context);
    }

    public CustomCard(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgLike:
                isClicked = !isClicked;
                imgLike.setImageResource(isClicked ? R.mipmap.ic_favorite_on : R.mipmap.ic_favorite_off);
                Versus versusClicked = (Versus)v.getTag();
                versusClicked.setIsLiked((isClicked) ? 1 : 0);
                versusClicked.save();

                addOrDeleteFromFavorite(isClicked, versusClicked);

                break;
            case R.id.imgShare:
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(Intent.EXTRA_TEXT, mShareText);
                mContext.startActivity(Intent.createChooser(sharingIntent, "title"));
                break;
        }
    }

    /**
     * Deletes/adds from database and remove from view
     */
    private void addOrDeleteFromFavorite(boolean isClicked, Versus versus) {
        // Add versus to Favorites;
        if (isClicked) {
            if (!mFavoriteTheme.versuses().contains(versus)) {
                Versus versusFavorite = new Versus();
                versusFavorite.themeId = String.valueOf(UtilsApp.FAVORITE_THEME_ID);
                versusFavorite.versusContent = versus.versusContent;
                versusFavorite.setIsLiked(1);
                versusFavorite.save();
            }
        } else {
            // Delete card
            if ((mVersus.themeId).equals(String.valueOf(UtilsApp.FAVORITE_THEME_ID))) {
                linearLayoutParentCard.removeView(CustomCard.this);
            }
            // Delete from database
            new Delete()
                    .from(Versus.class)
                    .where("theme_id = ? and versusContent = ?", UtilsApp.FAVORITE_THEME_ID, versus.versusContent)
                    .execute();
            // Update isLike in original theme and favoriteTheme
            new Update(Versus.class)
                    .set("isLiked = ?", 0)
                    .where("versusContent = ?", versus.versusContent)
                    .execute();
            requestLayout();
        }
    }

    private void publishTweet(String bibleVersus) {
        final StatusesService statusesService = Twitter.getInstance().getApiClient().getStatusesService();
        statusesService.update(bibleVersus, null, null, null, null, null, null, null,null, new Callback<Tweet>() {
            @Override
            public void success(Result<Tweet> tweetResult) {
                Toast.makeText(mContext, "Успешно опубликовали статус",
                        Toast.LENGTH_SHORT).show();
            }

            @Override
            public void failure(TwitterException e) {
                Toast.makeText(mContext, "Ошибка при отправке твита",
                        Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }
        });
    }
}
