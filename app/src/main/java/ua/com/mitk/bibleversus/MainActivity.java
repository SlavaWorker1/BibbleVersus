package ua.com.mitk.bibleversus;

import android.app.FragmentTransaction;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.view.WindowManager;

import com.activeandroid.ActiveAndroid;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;

import io.fabric.sdk.android.Fabric;


public class MainActivity extends AppCompatActivity {

    // Note: Your consumer key and secret should be obfuscated in your source code before shipping.
    private static final String TWITTER_KEY = "9W1YAnLK9PrbsnfIi1qUgYN3W";
    private static final String TWITTER_SECRET = "g1KHgmPyk9RhnMFyIiYIWkonSfRtsD6NpTr0sZf2haS865FlEi";


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        TwitterAuthConfig authConfig = new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);
        Fabric.with(this, new Twitter(authConfig));
//        initialize ActiveAndroid orm for work with database
        ActiveAndroid.initialize(this);

        setContentView(R.layout.activity_main);

        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.main_screen_fragment, new MainFragment(), "Fragment");
        transaction.commit();
    }

}
