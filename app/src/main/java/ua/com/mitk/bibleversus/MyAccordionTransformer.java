package ua.com.mitk.bibleversus;

import android.support.v4.view.ViewPager;
import android.view.View;

/**
 * Created by slava on 26.02.16.
 */
public class MyAccordionTransformer extends MyBaseTransform {

    @Override
    protected void onTransform(View page, float position) {
        page.setPivotX(position < 0 ? 0 : page.getWidth());
        page.setScaleX(position < 0 ? 1f + position : 1f - position);
    }
}
