package ua.com.mitk.bibleversus;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by slava on 16.11.16.
 */

public class SpinnerAdapter extends BaseAdapter {

    private List<String> poemList;

    private Context context;
    private LayoutInflater inflater;

    public SpinnerAdapter(Context context, List<String> poemList) {
        this.context = context;
        this.poemList = poemList;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return poemList.size();
    }

    @Override
    public Object getItem(int position) {
        return poemList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (convertView == null)
            view = inflater.inflate(R.layout.spinner_item, null);

        TextView text1 = (TextView) view.findViewById(R.id.tvSpinner);
        text1.setText(String.valueOf(poemList.get(position)));
        text1.setSelected(true);
        return view;
    }



}
